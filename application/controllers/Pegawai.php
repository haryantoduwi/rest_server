<?php
//DATABASE PEGAWAI
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\rest;

class Pegawai extends REST_Controller {
     private $db22;
    function __construct($config = 'rest') {
        parent::__construct($config);
        //$this->load->database();
         $this->db2 = $this->load->database('db_pegawai', TRUE);
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $kontak = $this->db2->get('db_pegawai')->result();
        } else {
            $this->db2->where('idpeg', $id);
            $kontak = $this->db2->get('db_pegawai')->result();
        }
        $this->response($kontak, 200);
    }
    //SIMPAN
    function index_post(){
        $data=array(
            'nama'=>$this->post('nama'),
            'pendidikantertinggi'=>$this->post('pendidikan'),            
            );
        $insert= $this->db2->insert('db_pegawai',$data);
        if($insert){
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail',502));
        }
    }
    //UPDATE
    function index_put(){
        $id=$this->put('id');
        $data=array(
                // 'id'=>$this->put('id'),
                'nama'=>$this->put('nama'),
                'pendidikantertinggi'=>$this->put('pendidikan'),
            );
        $this->db2->where('idpeg',$id);
        $update=$this->db2->update('db_pegawai',$data);
        if($update){
            $this->response($data,200);
        }else{
            $this->response(array('status'=> 'fail',502));
        }
    }
    //HAPUS
    function index_delete(){
        $id=$this->delete('id');
        $this->db2->where('idpeg',$id);
        $delete=$this->db2->delete('db_pegawai');
        if($delete){
            $this->response(array('status'=>'success'),200);
        }else{
            $this->reponse(array('status'=>'fail',502));
        }
    }
    //Masukan function selanjutnya disini
}
?>