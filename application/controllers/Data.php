<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
//use Restserver\Libraries\rest;
//use Restserver\Libraries\REST_Controller;

class Data extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        //$this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $kontak = $this->db->get('anggota_temp')->result();
        } else {
            $this->db->where('id', $id);
            $kontak = $this->db->get('anggota_temp')->result();
        }
        $this->response($kontak, 200);
    }
    //SIMPAN
    function index_post(){
        // $nama="Haryanto Stark";
        // $email="strak.haryanto@gmail.com";
        // $no_hp="085725818424";

        $data=array(
            //'id'=>$this->post('id'),
            'nama'=>$this->post('nama'),
            'email'=>$this->post('email'),
            'no_hp'=>$this->post('no_hp'), 
            'jenis_kelamin'=>$this->post('jenis_kelamin'),           
            );
        $insert= $this->db->insert('anggota_temp',$data);
        if($insert){
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail',502));
        }
    }
    //UPDATE
    function index_put(){
        $id=$this->put('id');
        $data=array(
            'nama'=>$this->put('nama'),
            'email'=>$this->put('email'),
            'no_hp'=>$this->put('no_hp'), 
            'jenis_kelamin'=>$this->put('jenis_kelamin'), 
            );
        $this->db->where('id',$id);
        $update=$this->db->update('anggota_temp',$data);
        if($update){
            $this->response($data,200);
        }else{
            $this->response(array('status'=> 'fail',502));
        }
    }
    //HAPUS
    function index_delete(){
        $id=$this->delete('id');
        $this->db->where('id',$id);
        $delete=$this->db->delete('anggota_temp');
        if($delete){
            $this->response(array('status'=>'success'),200);
            echo $id;
        }else{
            $this->reponse(array('status'=>'fail',502));
        }
    }
    //Masukan function selanjutnya disini
}
?>